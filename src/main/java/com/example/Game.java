package com.example;

import java.util.*;

public class Game {

    public static final String PLAYER_ONE = "X";
    public static final String PLAYER_TWO = "O";
    public static final String EMPTY_BOARD = "---\n---\n---";
    public static final String DRAW = "Draw";
    public static final int DIMENSION = 3;
    private String next = PLAYER_ONE;
    private final Set<Integer> moves = new HashSet<>();
    private String currentBoard = EMPTY_BOARD;

    public String getCurrentBoard() {
        return currentBoard;
    }

    public String next() {
        return next;
    }

    private String current() {
        return PLAYER_ONE.equals(next()) ? PLAYER_TWO : PLAYER_ONE;
    }

    public String play(Integer i) {
        if (!this.winner().isEmpty()) {
            throw new GameOverException();
        }
        next = PLAYER_ONE.equals(next) ? PLAYER_TWO : PLAYER_ONE;
        if (!moves.contains(i)) {
            moves.add(i);
            buildNewBoard(i);
            return currentBoard;
        }
        throw new TakenSquareException();
    }

    private void buildNewBoard(Integer i) {
        String boardWithoutLineBreaks = getBoardWithoutLineBreaks();
        String temp = boardWithoutLineBreaks.substring(0, i) + current() +
                boardWithoutLineBreaks.substring(i+1);
        temp = temp.substring(0, DIMENSION) + "\n" + temp.substring(DIMENSION, DIMENSION * 2) + "\n" + temp.substring(DIMENSION * 2);
        this.currentBoard = temp;
    }

    private String getBoardWithoutLineBreaks() {
        return this.currentBoard.replaceAll("\n", "");
    }

    public String winner() {
        if (isWinner(PLAYER_ONE)) return PLAYER_ONE;
        if (isWinner(PLAYER_TWO)) return PLAYER_TWO;
        if (allSquaresTaken()) return DRAW;
        return "";
    }

    private boolean isWinner(String player) {
        String win = player.repeat(DIMENSION);
        var winRow = extractRows().stream().filter(row -> row.equals(win)).findAny();
        if (winRow.isPresent()) {
            return true;
        }
        var winCol = extractColumns().stream().filter(col -> col.equals(win)).findAny();
        if (winCol.isPresent()) {
            return true;
        }
        var winDiag = extractDiagonals().stream().filter(diag -> diag.equals(win)).findAny();
        return winDiag.isPresent();
    }

    private List<String> extractDiagonals() {
        List<String> diagonals = new ArrayList<>();
        String board = getBoardWithoutLineBreaks();
        diagonals.add(charAtIndex(board, 0) + charAtIndex(board, 4) + charAtIndex(board,8));
        diagonals.add(charAtIndex(board, 2) + charAtIndex(board, 4) + charAtIndex(board,6));
        return diagonals;
    }

    private List<String> extractColumns() {
        List<String> columns = new ArrayList<>();
        String board = getBoardWithoutLineBreaks();
        columns.add(charAtIndex(board, 0) + charAtIndex(board, 3) + charAtIndex(board,6));        
        columns.add(charAtIndex(board, 1) + charAtIndex(board, 4) + charAtIndex(board,7));
        columns.add(charAtIndex(board, 2) + charAtIndex(board, 5) + charAtIndex(board,8));
        return columns;
    }

    private List<String> extractRows() {
        List<String> rows = new ArrayList<>();        
        rows.add(getBoardWithoutLineBreaks().substring(0, 3));
        rows.add(getBoardWithoutLineBreaks().substring(3, 6));
        rows.add(getBoardWithoutLineBreaks().substring(6));
        return rows;
    }

    private boolean allSquaresTaken() {
        return (!this.getBoardWithoutLineBreaks().contains("-"));
    }

    private String charAtIndex(String s, Integer i) {
        return s.substring(i, i+1);
    }

    private boolean squareIs(Integer i, String player) {
        return Objects.equals(player, charAtIndex(this.getBoardWithoutLineBreaks(), i));
    }
}
