import com.example.Game;
import com.example.GameOverException;
import com.example.TakenSquareException;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class TicTacToeTests {

    @Test
    public void should_initialize_game_with_empty_board() {
        Game game = new Game();
        assertEquals(game.getCurrentBoard(), "---\n---\n---");
    }

    @Test
    public void should_expect_next_move_from_X() {
        Game game = new Game();
        assertEquals(game.next(), "X");
    }

    @Test
    public void should_accept_next_move_from_X() {
        Game game = new Game();
        assertEquals(game.play(0), "X--\n---\n---");
    }

    @Test
    public void should_expect_next_move_from_O() {
        Game game = new Game();
        game.play(0);
        assertEquals(game.next(), "O");
    }

    @Test
    public void should_expect_next_move_from_X_after_O_played() {
        Game game = new Game();
        game.play(0);
        game.play(1);
        assertEquals(game.next(), "X");
    }

    @Test
    public void should_expect_next_move_from_O_after_3_moves_played() {
        Game game = new Game();
        game.play(0);
        game.play(1);
        game.play(2);
        assertEquals(game.next(), "O");
    }

    @Test
    public void should_not_accept_move_in_a_taken_square() {
        Game game = new Game();
        game.play(0);
        assertThrows(TakenSquareException.class, () -> game.play(0));
    }

    @Test
    public void should_accept_move_in_an_empty_square() {
        Game game = new Game();
        game.play(0);
        assertEquals(game.play(1), "XO-\n---\n---");
    }

    @Test
    public void game_should_say_X_is_winner_if_X_wins() {
        Game game = new Game();
        game.play(0);
        game.play(2);
        game.play(3);
        game.play(5);
        game.play(6);
        assertEquals("X", game.winner());
    }

    @Test
    public void game_should_not_say_X_is_winner_if_X_does_not_win() {
        Game game = new Game();
        game.play(0);
        game.play(2);
        game.play(3);
        game.play(5);
        assertEquals("", game.winner());
    }

    @Test
    public void game_should_say_O_is_winner_if_O_wins() {
        Game game = new Game();
        game.play(0);
        game.play(2);
        game.play(3);
        game.play(5);
        game.play(1);
        game.play(8);
        assertEquals("O", game.winner());
    }

    @Test
    public void game_should_stop_if_isDraw() {
        Game game = new Game();
        game.play(0);
        game.play(1);
        game.play(2);
        game.play(3);
        game.play(5);
        game.play(8);
        game.play(4);
        game.play(6);
        game.play(7);
        assertEquals("Draw", game.winner());
    }

    @Test
    public void game_should_stop_if_X_wins() {
        Game game = new Game();
        game.play(0);
        game.play(2);
        game.play(3);
        game.play(5);
        game.play(6);
        assertThrows(GameOverException.class, () -> game.play(7));
    }

    @Test
    public void game_should_stop_if_O_wins() {
        Game game = new Game();
        game.play(0);
        game.play(2);
        game.play(3);
        game.play(5);
        game.play(1);
        game.play(8);
        assertThrows(GameOverException.class, () -> game.play(4));
    }
}
